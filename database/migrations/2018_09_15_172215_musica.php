<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Musica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('musica', function( Blueprint $table ){
            $table->increments('id');
            $table->string('nomeMusica', 40);

            $table->integer('id_banda')->unsigned();
            $table->foreign('id_banda')->references('id')->on('banda');
            $table->integer('id_compositor')->unsigned();
            $table->foreign('id_compositor')->references('id')->on('compositor');
            $table->integer('id_genero')->unsigned();
            $table->foreign('id_genero')->references('id')->on('genero');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::drop('musica');
    }
}
