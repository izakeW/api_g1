<?php

use Illuminate\Database\Seeder;
use App\Compositor;

class CompositorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Compositor::create(['nomeCompositor'=>'Anthony Kiedis','idade'=> 55]);
        Compositor::create(['nomeCompositor'=>'Humberto Gessinger','idade'=> 54]);
        Compositor::create(['nomeCompositor'=>'Fernando de Ouro Preto','idade'=> 54]);
        Compositor::create(['nomeCompositor'=>'Samuel Rosa','idade'=> 56]);
        Compositor::create(['nomeCompositor'=>'John Lennon','idade'=> 40]);

    }
}
