<?php

use Illuminate\Database\Seeder;
use App\musica;
class MusicaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Musica::create([ 'nomeMusica'=>'Primeiros erros', 'id_banda'=>2, 'id_compositor'=>3 , 'id_genero'=>1]);
        Musica::create([ 'nomeMusica'=>'Dom quixote', 'id_banda'=>4, 'id_compositor'=>2 , 'id_genero'=>1]);
        Musica::create([ 'nomeMusica'=>'Come together', 'id_banda'=>5, 'id_compositor'=>5 , 'id_genero'=>1]);
        Musica::create([ 'nomeMusica'=>'Vamos fugir', 'id_banda'=>1, 'id_compositor'=>4 , 'id_genero'=>6]);
        Musica::create([ 'nomeMusica'=>'Dani california', 'id_banda'=>3, 'id_compositor'=>1 , 'id_genero'=>1]);
    }
}
