<?php

use Illuminate\Database\Seeder;
use App\Genero;

class GeneroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Genero::create(['descricao'=>'Rock']);
        Genero::create(['descricao'=>'Alternativo']);
        Genero::create(['descricao'=>'Indie']);
        Genero::create(['descricao'=>'Pop']);
        Genero::create(['descricao'=>'Jazz']);
        Genero::create(['descricao'=>'Reggae']);
    }
}
