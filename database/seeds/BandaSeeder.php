<?php

use Illuminate\Database\Seeder;
use App\Banda;
class BandaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Banda::create(['nomeBanda'=>'Skank','integrantes'=>5]);

        Banda::create(['nomeBanda'=>'Capital inicial','integrantes'=>6]);

        Banda::create(['nomeBanda'=>'Red hot chili peppers','integrantes'=>4]);

        Banda::create(['nomeBanda'=>'Engenheiros do Hawaii','integrantes'=>3]);

        Banda::create(['nomeBanda'=>'Beatles','integrantes'=>4]);

    }
}
