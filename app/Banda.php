<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banda extends Model
{
    //
    protected $table = 'banda';
    protected $fillable = ['nomeBanda','integrantes'];

     public function musica(){
        return $this->hasMany('App\Musica', 'id_banda', 'id');
    }
}
