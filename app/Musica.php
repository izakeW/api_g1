<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Musica extends Model
{
    //
    protected $table = 'musica';

    protected $fillable = ['nomeMusica','id_banda','id_compositor', 'id_genero'];

    public function banda(){
    	return $this->belongsTo('App\Banda','id_banda','id');
    }

    public function compositor(){
    	return $this->belongsTo('App\Compositor','id_compositor','id');
    }
    
    public function genero(){
    	return $this->belongsTo('App\Genero','id_genero','id');
    }
}
