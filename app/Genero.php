<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genero extends Model
{
    //
        protected $table = 'genero';
        protected $fillable = ['descricao'];

        public function musica(){
        return $this->hasMany('App\Musica', 'id_genero', 'id');
    }
}
