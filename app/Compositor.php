<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compositor extends Model
{
    //
    protected $table = 'compositor';
    protected $fillable = ['nomeCompositor', 'idade'];

    public function musica(){
        return $this->hasMany('App\Musica', 'id_compositor', 'id');
    }
}
