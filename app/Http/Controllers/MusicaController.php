<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Musica;
use Validator;
use Mockery\Exception;

class MusicaController extends Controller
{

    private $atributos = ['nomeMusica','id_banda','id_compositor','id_genero'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(request $request)
    {
        //
         //return Musica::paginate();
        $qtd = $request->input('qtd');
         try{
            return response()->json( [Musica::paginate($qtd)], 200 );
        }catch( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $validacao = $this->validar($request);

            if ($validacao->fails()) {
                return response()->json([
                    'mensagem' => 'Erro',
                    'erros' => $validacao->errors()
                ], 400);
            }

        $musica = new Musica();
        $musica->fill($request->all());
        $musica->save();
        return $musica;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //$musica = Musica::find($id);
        //return $musica;

        //$musica = Musica::with(['banda', 'compositor', 'genero' ])->find($id);
        //return $musica;

        try{

            if( $id > 0 ){
                $musica = Musica::with(['banda', 'compositor', 'genero' ])->find($id);
                    return $musica;
                if( $musica ){
                    return $musica;
                }else{
                    return response()->json( ["mensagem" => "Registro nao encontrado"], 404 );
                }
            }else{
                return response()->json( ["mensagem" => "Parametro invalido"], 400 );
            }


        }catch ( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validacao = $this->validar($request);

            if ($validacao->fails()) {
                return response()->json([
                    'mensagem' => 'Erro',
                    'erros' => $validacao->errors()
                ], 400);
            }

        $musica = Musica::find($id);
        $musica->fill($request->all());
        $musica->save();
        return $musica;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $musica = Musica::find($id);
        $musica->delete();
        return $musica;
    }

    public function validar( $request ){

        $validator = Validator::make($request->only( $this->atributos ),[
            'nomeMusica' => 'required|min:3|max:40',
            'id_banda' => 'required|numeric',
            'id_compositor' => 'required|numeric',
            'id_genero' => 'required|numeric'

        ]);



        return $validator;
    }
}
