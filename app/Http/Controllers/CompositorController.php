<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Compositor;
use Validator;
use Mockery\Exception;

class CompositorController extends Controller
{

    private $atributos = ['nomeCompositor','idade'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(request $request)
    {
        //
         //return Musica::paginate();
        $qtd = $request->input('qtd');
         try{
            return response()->json( [Compositor::paginate($qtd)], 200 );
        }catch( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validacao = $this->validar($request);

            if ($validacao->fails()) {
                return response()->json([
                    'mensagem' => 'Erro',
                    'erros' => $validacao->errors()
                ], 400);
            }
        $compositor = new Compositor();
        $compositor->fill($request->all());
        $compositor->save();
        return $compositor;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try{

            if( $id > 0 ){
                $compositor = Compositor::with('musica')->find($id);
                    return $compositor;
                if( $compositor ){
                    return $compositor;
                }else{
                    return response()->json( ["mensagem" => "Registro nao encontrado"], 404 );
                }
            }else{
                return response()->json( ["mensagem" => "Parametro invalido"], 400 );
            }


        }catch ( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validacao = $this->validar($request);

            if ($validacao->fails()) {
                return response()->json([
                    'mensagem' => 'Erro',
                    'erros' => $validacao->errors()
                ], 400);
            }
        $compositor = Compositor::find($id);
        $compositor->fill($request->all());
        $compositor->save();
        return $compositor;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $compositor = Compositor::find($id);
        $compositor->delete();
        return $compositor;
    }


    public function validar( $request ){

        $validator = Validator::make($request->only( $this->atributos ),[
            'nomeCompositor' => 'required|min:3|max:50',
            'idade' => 'required|numeric',

        ]);
        return $validator;
    }
}
